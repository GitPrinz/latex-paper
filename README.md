# Dummy Paper

*Draft for a Paper Repo. Update this Readme to your needs, change the folder names, ... 
Comments on how to improve this are welcome.*

See also [Other LaTeX Drafts](https://gitlab.com/GitPrinz/latex)

[TOC]

## Overview

- [docker](docker): Information to create an environment for compilation
- [document](document): Paper related LaTeX files
  - [paper_main.tex](paper_main.tex): Main file for paper
    *Probably you want to rename it, to make it more unique.*  
    *Make sure to change it also in the other documents.*
    -  [![PDF](https://img.shields.io/badge/Download-PDF-green)](https://gitlab.com/GitPrinz/latex-paper/-/jobs/artifacts/master/raw/document/paper_main.pdf?job=compile) or [.dox version](https://gitlab.com/GitPrinz/latex-paper/-/jobs/artifacts/master/raw/document/paper_main.docx?job=docfile) (master branch)  
  - [abstract.tex](abstract.tex)
  - [classification_keywords.tex](classification_keywords.tex)
  - [introduction.tex](introduction.tex)
  - [background_methods.tex](background_methods.tex)
  - [conclusion_summary.tex](conclusion_summary.tex)
  - [acknowledgements_references.tex](acknowledgements_references.tex)
- [figures](figures): All previously prepared and therefore static images for this paper
- [results](results): Code to generate more figures dynamically from data
  *Change/Add python files and update the Makefile to create what you need.*
- [Documentation.md](Documentation.md): Descriptions how to recreate the results (Just a Placeholder right now)

### General Information

*Over time you probably want to create more folders for different conferences or material like posters or presentations. In this case it is helpful to store general files at the top level.*

- [acronym_defenition.tex](acronym_defenition.tex): All the used acronyms
- [related_literature.bib](related_literature.bib): Related Literature
- [load_packages.tex](load_packages.tex): Loader for general necessary LaTeX Packages
- [meta_information.tex](meta_information.tex): for authors or other general information

### Automatically PDF creation

 [![Pipeline status](https://gitlab.com/GitPrinz/latex-paper/badges/master/pipeline.svg)](https://gitlab.com/GitPrinz/latex-paper/-/pipelines) (master)

This Repository contains a CI to automatically create the pdf document (and a .docx version).
It is prepared for dynamic figure generation.

Thanks very much to [Martin Isaksson](https://gitlab.com/martisak) for the [Code](https://gitlab.com/martisak/latex-pipeline)!

The .docx version is compiled via pandoc (see [MAKEFILE](MAKEFILE)). It uses some macros defined in [pandoc_docx_macros.tex](pandoc_docx_macros.tex) to improve the output.
However, it is only for collaboration and not presentable.

## Notes

*This part is for a meta discussion of the structure. Delete this in your Repo.*

My Structure intends to collect multiple paper versions (for different conferences or journals) and related posters and presentations at one place. Reused material like literature, images, acronyms, meta information and more should exist just once and therefore updates all related material if changed.

**Therefore this structure is topic related and not paper related!**

The separation of LaTeX files is just for my needs. If files are separated in different parts it is more easy to let multiple people work on it. On the other hand, I combined some unrelated parts to not have it splintered too much. Git merge tools are usually very powerful.

### Drawbacks of topic related Structure

If you want to preserve a unique version for one conference you can use a tag, but if you later change shared information this will influence also "old" versions. Shared data on top level should be not unique for  single conferences or papers.

Some journals or conferences expect the LaTeX Files to be all in one folder, or in subfolders relative to the main file. This is not possible with this general approach where it is planed to use the same files in multiple documents. In this case, I create a subfolder with the submission material.

