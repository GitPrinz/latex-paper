.DEFAULT_GOAL := help
###########################################################################################################
## SCRIPTS
###########################################################################################################

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
        match = re.match(r'^([,\sa-zA-Z_-]+):.*?## (.*)$$', line)
        if match:
                target, help = match.groups()
                print("%-20s %s" % (target, help))
endef


###########################################################################################################
## VARIABLES
###########################################################################################################

ifeq ($(OS),Windows_NT)
    export PYTHON=py
else
    export PYTHON=python3
endif

export PRINT_HELP_PYSCRIPT


###########################################################################################################
## TARGETS
###########################################################################################################

# credit to https://gitlab.com/martisak/latex-pipeline

SOURCES=$(wildcard ./*/*main.tex)  # find "...main.tex" files in all subfolders
PDF_OBJECTS=$(SOURCES:.tex=.pdf)  # create list with pdf targets

# Options #
###########

LATEXMK=latexmk
LATEXMK_OPTIONS=-f -bibtex -pdf -pdflatex="pdflatex -interaction=nonstopmode"

DOCKER=docker
DOCKER_COMMAND=run --rm -w /data/ --env LATEXMK_OPTIONS_EXTRA=$(LATEXMK_OPTIONS_EXTRA)
DOCKER_MOUNT=-v`pwd`:/data

help:
	@$(PYTHON) -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

all: render

pdf: $(PDF_OBJECTS)  ## create all pdfs

%.pdf: %.tex  # all pdf targets depend on their tex file.
	@echo Input file: $<
	cd $(dir $<) &&	$(LATEXMK) $(LATEXMK_OPTIONS_EXTRA) $(LATEXMK_OPTIONS) $(notdir $<)

docx:  ## Create a Word dcoument from main document file:
	cd document && pandoc -s --bibliography=../related_literature.bib -o paper_main.docx ../pandoc_docx_macros.tex paper_main.tex


clean:  ## remove automatically generated LaTeX files
	cd document && $(LATEXMK) -bibtex -C
	-rm -f document/paper_main.docx

dist-clean: clean
	-rm $(FILENAME).tar.gz

render:
	$(DOCKER) $(DOCKER_COMMAND) $(DOCKER_MOUNT) blang/latex:ctanfull \
		make pdf

install:  ## install reuirements
		cd results && $(MAKE) install

results:  ## create results
		cd results && $(MAKE) figures

debug:
	$(DOCKER) $(DOCKER_COMMAND) -it $(DOCKER_MOUNT) blang/latex:ctanfull \
		bash

check_docker:
	$(DOCKER) $(DOCKER_COMMAND) -it $(DOCKER_MOUNT) ruby:2.7.1 \
		bundle update --bundler; make check

.PHONY: results


# Docker Stuff #
################
# Will probably not work, but can be used as guideline

docker-image:  ## Create image to run Python and LaTeX
	docker build -t latex-python docker/
	
docker-run:  ## start docker
	# it=interactive, rm=remove environment after exit
	docker run --rm --volume ${PWD}:/work --workdir /work -it latex-python
	
docker-push:  ## push docker image to gitlab
	docker login registry.gitlab.com
	docker build -t registry.gitlab.com/gitprinz/latex-paper docker/
	docker push registry.gitlab.com/gitprinz/latex-paper