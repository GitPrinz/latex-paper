# Preparation #
###############

# Base is simple Linux version
FROM ubuntu:latest

# Set Timezone (for texlive install)
ENV TZ=Europe/Berlin
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Prepare Linux
RUN apt-get update -y
RUN apt-get upgrade -y

# Install wget
RUN apt-get install wget -y

# Install Make
RUN apt-get install make -y

# Further Utilities (pdftotext, ...)
RUN apt-get install poppler-utils -y


# Python #
##########

# Install Python, PIP
RUN apt-get install -y python3
RUN apt-get install -y python3-pip

# Install Result Requirements
# 	Lazy, but easy (better would be to include requirements or even use the make files)
RUN pip3 install pandas seaborn


# LaTeX #
#########

# Install LaTeX
# "texlive-font-utils" needed for epsconversion (repstopdf)
RUN apt-get install texlive-lang-german texlive-latex-extra texlive-font-utils -y

# Install latexmk to run latex compiling
RUN apt-get install latexmk -y

# Install pandoc for special latex compiling
RUN apt-get install pandoc pandoc-citeproc -y

# Install more packages
# type1ec.sty
RUN apt-get install cm-super -y
# siunitx.sty
RUN apt-get install texlive-science -y
# dvipng
RUN apt-get install dvipng -y

# make tlmgr working
RUN tlmgr init-usertree
RUN tlmgr repository add ftp://tug.org/historic/systems/texlive/2019/tlnet-final
RUN tlmgr repository remove http://mirror.ctan.org/systems/texlive/tlnet
RUN tlmgr option repository ftp://tug.org/historic/systems/texlive/2019/tlnet-final

# Install more packages for document via tlmgr
# acmart.cls
RUN tlmgr --verify-repo=none install acmart



# Finished